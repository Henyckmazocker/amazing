package tastat;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "client")
public class Client{
	
	@Id
	@Column(name = "CIF", length=10)
	protected String CIF;
	@Column(name="nom", nullable = false)
	protected String nomClient;
	@Column(name = "actiu", nullable = false)
	protected boolean actiu;
	
	@JoinColumn(name="address", nullable=false)		//relacio 1 a 1 amb adreça
	@OneToOne(cascade = CascadeType.PERSIST)
	protected Address address;
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@OneToMany(mappedBy="client", fetch = FetchType.EAGER, cascade=CascadeType.MERGE)	//relacio 1 a n amb comanda
	Set<Comanda> comandes = new HashSet<Comanda>();
	
	public Set<Comanda> getComandes() {
		return comandes;
	}
	public void setComandes(Set<Comanda> comandes) {
		this.comandes = comandes;
	}
	public Client(String cIF, String nomClient, boolean actiu) {
		super();
		CIF = cIF;
		this.nomClient = nomClient;
		this.actiu = actiu;
	}
	public Client() {
		super();
	}
	public String getCIF() {
		return CIF;
	}
	public void setCIF(String cIF) {
		CIF = cIF;
	}
	public String getNomClient() {
		return nomClient;
	}
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}
	public boolean isActiu() {
		return actiu;
	}
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	
	
	

}