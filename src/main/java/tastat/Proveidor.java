package tastat;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "proveidor")
public class Proveidor{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idProveidor")
	protected int idProveidor;
	@Column(name="nom")
	protected String nomProveidor;
	@Column(name="CIF", nullable = false)
	protected String CIF;
	@Column(name="actiu")
	protected boolean actiu;
	@Column(name="contacte")
	protected String personaContacte;
	
	@OneToMany(cascade = { CascadeType.ALL },mappedBy="proveidor")
	private Set<PeticioProveidor> peticio = new HashSet();
	
	@JoinColumn(name="address", nullable=false)
	@OneToOne(cascade = CascadeType.PERSIST)
	protected Address address;
	
	public Proveidor(int idProveidor, String nomProveidor, String cIF, boolean actiu, String personaContacte) {
		super();
		this.idProveidor = idProveidor;
		this.nomProveidor = nomProveidor;
		CIF = cIF;
		this.actiu = actiu;
		this.personaContacte = personaContacte;
	}
	public int getIdProveidor() {
		return idProveidor;
	}
	public void setIdProveidor(int idProveidor) {
		this.idProveidor = idProveidor;
	}
	public String getNomProveidor() {
		return nomProveidor;
	}
	public void setNomProveidor(String nomProveidor) {
		this.nomProveidor = nomProveidor;
	}
	public String getCIF() {
		return CIF;
	}
	public void setCIF(String cIF) {
		CIF = cIF;
	}
	public boolean isActiu() {
		return actiu;
	}
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	public String getPersonaContacte() {
		return personaContacte;
	}
	public void setPersonaContacte(String personaContacte) {
		this.personaContacte = personaContacte;
	}
	
	
	//relacio 1 a 1 amb address
	//relacio 1 a n amb peticioproveidor
	
}