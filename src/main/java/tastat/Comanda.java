package tastat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "comanda")
public class Comanda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idComanda")
	protected int idComanda;
	@Temporal(TemporalType.DATE)
	@Column(name = "dataComanda", nullable = false)
	protected Date dataComanda;
	@Temporal(TemporalType.DATE)
	@Column(name = "dataLliurament")
	protected Date dataLliurament;
	@Enumerated(EnumType.STRING)
	@Column(name = "estat", nullable = false) 
	protected ComandaEstat estat; // PENDENT - 0PREPARAT - TRANSPORT - LLIURAT

	@ManyToMany(cascade = { CascadeType.ALL },mappedBy="comandas")
	private Set<Producte> producte = new HashSet();

	public Set<Producte> getProducte() {
		return producte;
	}
	public void setProducte(Set<Producte> producte) {
		this.producte = producte;
	}

	@ManyToOne
	@JoinColumn(name = "client")
	private Client client;

	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Comanda(Date dataComanda, Date dataLliurament, ComandaEstat estat, Client c) {
		super();
		this.dataComanda = dataComanda;
		this.dataLliurament = dataLliurament;
		this.estat = estat;
	}
	public Comanda() {
		super();
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	// relacio n a n amb producte
	// relacio n a 1 amb client
}