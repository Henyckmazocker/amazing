package tastat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "producte")
public class Producte{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codi")
	protected int codiProducte;
	@Column(name = "nom", nullable = false)
	protected String nomProducte;
	@Column(name = "stock")
	protected int stock;
	@Column (name = "stockMinim", nullable = false)
	protected int stockMinim;
	@Enumerated(EnumType.STRING)
	@Column(name ="unitat", nullable = false)
	protected UnitatMesura unitat;
	@Enumerated(EnumType.STRING)
	@Column(name="tipus", nullable = false)
	protected Tipus tipus;
	@Column(name="preu", nullable = false)
	protected double preuVenda;
	
	@OneToMany(mappedBy="producte",fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
	Set<Lot> lots = new HashSet<Lot>();
	
	public Set<Lot> getLots() {
		return lots;
	}
	public void setLots(Set<Lot> lots) {
		this.lots = lots;
	}
	@OneToMany(mappedBy="producte", cascade = CascadeType.REMOVE)
	Set<PeticioProveidor> pp = new HashSet<PeticioProveidor>();
	
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name="components",
	joinColumns= {@JoinColumn (name="codi", referencedColumnName="codi")},
	inverseJoinColumns= {@JoinColumn(name="codiComponent",referencedColumnName="codi")})
	private Set<Producte> prod = new HashSet();
	
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name="productecomanda",
	joinColumns= {@JoinColumn (name="codi", referencedColumnName="codi")},
	inverseJoinColumns= {@JoinColumn(name="idComanda",referencedColumnName="idComanda")})
	private Set<Comanda> comandas = new HashSet();
	
	public Producte() {
		super();
	}
	public Producte(String nomProducte, int stock, int stockMinim, UnitatMesura unitat, Tipus tipus,
			double preuVenda) {
		super();
		this.codiProducte=codiProducte;
		this.nomProducte = nomProducte;
		this.stock = stock;
		this.stockMinim = stockMinim;
		this.unitat = unitat;
		this.tipus = tipus;
		this.preuVenda = preuVenda;
	}
	public int getCodiProducte() {
		return codiProducte;
	}
	public void setCodiProducte(int codiProducte) {
		this.codiProducte = codiProducte;
	}
	public String getNomProducte() {
		return nomProducte;
	}
	public void setNomProducte(String nomProducte) {
		this.nomProducte = nomProducte;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getStockMinim() {
		return stockMinim;
	}
	public void setStockMinim(int stockMinim) {
		this.stockMinim = stockMinim;
	}
	public UnitatMesura getUnitat() {
		return unitat;
	}
	public void setUnitat(UnitatMesura unitat) {
		this.unitat = unitat;
	}
	public Tipus getTipus() {
		return tipus;
	}
	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}
	public double getPreuVenda() {
		return preuVenda;
	}
	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}
	
	
	//relacio 1 a n amb lot
	//relacio n a n amb ell mateix
	//relacio 1 a n amb peticionsproveidor
	//relacio n a n amb comanda
	
	
	
	
}