package tastat;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "peticio_proveidor")
public class PeticioProveidor {
	
	public PeticioProveidor() {
		super();
	}
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idComanda")
	protected int idComanda;
	@Column(name = "tipusPeticio", nullable = false)
	protected int tipusPeticio;
    @Temporal(TemporalType.DATE)
	@Column(name = "dataOrdre", nullable = false)
	protected Date dataOrdre;
    @Temporal(TemporalType.DATE)
	@Column(name ="dataRecepcio")
	protected Date dataRecepcio;
	@Enumerated(EnumType.STRING)
	@Column(name="estat")
	protected ComandaEstat estat;
	
	@ManyToOne
	@JoinColumn(name = "proveidor")
	private Proveidor proveidor;
	
	@ManyToOne
	@JoinColumn(name = "producte")
	private Producte producte;
	
	public PeticioProveidor( Producte prod,Proveidor prove,int tipusPeticio, Date dataOrdre, Date dataRecepcio, ComandaEstat estat) {
		super();
		this.idComanda = idComanda;
		this.tipusPeticio = tipusPeticio;
		this.dataOrdre = dataOrdre;
		this.dataRecepcio = dataRecepcio;
		this.estat = estat;
		this.producte=prod;
		this.proveidor=prove;
	}
	
	public Proveidor getProveidor() {
		return proveidor;
	}

	public void setProveidor(Proveidor proveidor) {
		this.proveidor = proveidor;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getIdComanda() {
		return idComanda;
	}
	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	public int getTipusPeticio() {
		return tipusPeticio;
	}
	public void setTipusPeticio(int tipusPeticio) {
		this.tipusPeticio = tipusPeticio;
	}
	public Date getDataOrdre() {
		return dataOrdre;
	}
	public void setDataOrdre(Date dataOrdre) {
		this.dataOrdre = dataOrdre;
	}
	public Date getDataRecepcio() {
		return dataRecepcio;
	}
	public void setDataRecepcio(Date dataRecepcio) {
		this.dataRecepcio = dataRecepcio;
	}
	public ComandaEstat getEstat() {
		return estat;
	}
	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}
	
	
	//relacio n a 1 amb proveidor
	//relacio n a 1 amb producte

}
