package tastat;

import java.sql.Date;

import org.hibernate.Session;
import BDDmodify.Utils;
import dao_not_copypasted.*;


public class Main {

	public static void main(String[] args) {
		
		ClientDao dao = new ClientDao();

		AddressDao adao = new AddressDao();

		ComandaDao cdao = new ComandaDao();

		Client c = new Client("1353532", "a", true);

		Address a = new Address(false, "alsfhblas", 0, null, null, null, null, 0, 0);

		Comanda c1 = new Comanda(new Date(1, 1, 1), new Date(2, 2, 2), Enum.valueOf(ComandaEstat.class, "PENDENT"), c);

			//daos client,comanda i adress
		c.setAddress(a);

		c.getComandes().add(c1);
		
		adao.saveOrUpdate(a);

		cdao.saveOrUpdate(c1);

		dao.saveOrUpdate(c);

		dao.delete(c.getCIF());
		
		//daos producte,lot
		ProducteDao pdao=new ProducteDao();
		LotDao ldao=new LotDao();
		
		Producte p=new Producte("cadaveres", 0, 10,Enum.valueOf(UnitatMesura.class, "UNITAT"), Enum.valueOf(Tipus.class, "VENDIBLE"), 100);
		
		Lot lot=new Lot(40, new Date(1,1,1), new Date(2,2,2));
		
		p.getLots().add(lot);
		
		pdao.saveOrUpdate(p);
		
		lot.setProducte(p);


		ldao.saveOrUpdate(lot);
	
		
		pdao.delete(p.getCodiProducte());
		
		//probando triggers
		
		Producte p2=new Producte("cadaveres", 0, 10,Enum.valueOf(UnitatMesura.class, "UNITAT"), Enum.valueOf(Tipus.class, "VENDIBLE"), 100);
		
		Lot lot2=new Lot(40, new Date(1,1,1), new Date(2,2,2));
		
		p2.getLots().add(lot2);
		
		pdao.saveOrUpdate(p2);
		
		lot2.setProducte(p2);
		ldao.afegirLot(lot2);

		ldao.saveOrUpdate(lot2);
		ldao.delete(lot2.getId());
		
		//Segundo interceptor (y ultimo)
		
		p2.getLots().add(lot2);
		
		c.setAddress(a);

		c.getComandes().add(c1);
		
		c1.getProducte().add(p2);
		
		adao.saveOrUpdate(a);

		cdao.saveOrUpdate(c1);

		dao.saveOrUpdate(c);		
		
	}
}