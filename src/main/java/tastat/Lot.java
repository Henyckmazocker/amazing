package tastat;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "lot")
public class Lot{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idLot")
	int id;
	@Column(name = "quantitat", nullable = false)
	int quantitat;
    @Temporal(TemporalType.DATE)
	@Column(name = "data_entrada", nullable = false)
	Date dataEntrada = new Date();
    @Temporal(TemporalType.DATE)
    @Column(name = "data_caducitat", nullable = false)
	Date dataCaducitat = null;
    
    @ManyToOne
	@JoinColumn(name = "producte")
	private Producte producte;
	
	public Producte getProducte() {
		return producte;
	}
	public void setProducte(Producte producte) {
		this.producte = producte;
	}
	
	
	public Lot() {
		super();
	}
	public Lot( int quantitat, Date dataEntrada, Date dataCaducitat) {
		super();
		this.id=id;
		this.quantitat = quantitat;
		this.dataEntrada = dataEntrada;
		this.dataCaducitat = dataCaducitat;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantitat() {
		return quantitat;
	}
	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
	public Date getDataEntrada() {
		return dataEntrada;
	}
	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	public Date getDataCaducitat() {
		return dataCaducitat;
	}
	public void setDataCaducitat(Date dataCaducitat) {
		this.dataCaducitat = dataCaducitat;
	}
	
	
	//relacio n a 1 amb producte
	
	
}