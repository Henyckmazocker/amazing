package tastat;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address{
	
	@Column(name = "principal")
	protected boolean principal;
	@Id
	@Column(name = "carrer", length = 50)
	protected String carrer;
	public Client getCliente() {
		return cliente;
	}
	public void setCliente(Client cliente) {
		this.cliente = cliente;
	}
	public Proveidor getProveidor() {
		return proveidor;
	}
	public void setProveidor(Proveidor proveidor) {
		this.proveidor = proveidor;
	}
	@Column(name = "numero")
	protected int numero;
	@Column(name = "poblacio")
	protected String poblacio;
	@Column(name = "pais")
	protected String pais;
	@Column(name = "postal")
	protected String postal;
	@Column(name = "telefon")
	protected String telefon;
	@Column(name = "latitud")
	protected double latitud;
	@Column(name = "longitud")
	protected double longitud;
	
	@JoinColumn(name="client")			//relacio 1 a 1 amb client
	@OneToOne(cascade = CascadeType.PERSIST)
	protected Client cliente;
	
	@JoinColumn(name="proveidor")	//relacio 1 a 1 amb proveidor
	@OneToOne(cascade = CascadeType.PERSIST)
	protected Proveidor proveidor;
	
	public Address(boolean principal, String carrer, int numero, String poblacio, String pais, String postal,
			String telefon, double latitud, double longitud) {
		super();
		this.principal = principal;
		this.carrer = carrer;
		this.numero = numero;
		this.poblacio = poblacio;
		this.pais = pais;
		this.postal = postal;
		this.telefon = telefon;
		this.latitud = latitud;
		this.longitud = longitud;
	}
	public Address() {
		super();
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	public String getCarrer() {
		return carrer;
	}
	public void setCarrer(String carrer) {
		this.carrer = carrer;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getPoblacio() {
		return poblacio;
	}
	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public double getLatitud() {
		return latitud;
	}
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	
	


}