package dao_not_copypasted;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import BDDmodify.Utils;
import tastat.Client;
import tastat.Comanda;
import tastat.Lot;

public class ClientDao extends GenericDao<Client, String> implements IClientDao {

	@Override
	public Client get(String cif) {

		return null;
	}

	@Override
	public void delete(String cif) {

		Session session = Utils.getSessionFactory().openSession();

	
		Client l = BDDmodify.Queries.veureClients(cif);

		System.out.println(l);

		ComandaDao dao = new ComandaDao();
		System.out.println(l.getComandes().size());

		session.beginTransaction();

		try {

			for (Comanda c : l.getComandes()) {

				dao.delete(c.getIdComanda());	

			}
			
			session.remove(l);
			session.getTransaction().commit();
			
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}

	}
}
