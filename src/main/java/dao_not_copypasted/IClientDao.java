package dao_not_copypasted;

import java.util.List;

import tastat.Client;

public interface IClientDao  extends IGenericDao<Client, String>{

	void saveOrUpdate(Client c);

	Client get(String cif);

	void delete(String cif);

	List<Client> list();
	
}
