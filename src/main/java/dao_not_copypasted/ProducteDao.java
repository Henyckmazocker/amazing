package dao_not_copypasted;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import BDDmodify.Utils;
import tastat.*;

public class ProducteDao extends GenericDao<Producte, Integer> implements IProducteDao {

	@Override
	public void delete(Integer n) {

		Session session = Utils.getSessionFactory().openSession();

		session.beginTransaction();
		
		Producte p = BDDmodify.Queries.veureProductes(n);

		LotDao ld = new LotDao();

		try {

			for (Lot l : p.getLots()) {
				ld.delete(l.getId());

			}

			session.remove(p);

			session.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}

	}

}
