package dao_not_copypasted;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import BDDmodify.Utils;
import tastat.*;

public class LotDao extends GenericDao<Lot, Integer> implements ILotDao {

	@Override
	public boolean afegirLot(Lot l) {

		Session session = Utils.getSessionFactory().openSession();

		int n = l.getProducte().getStock() + l.getQuantitat();

		l.getProducte().setStock(n);

		try {

			session.saveOrUpdate(l);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		}
	}

	@Override
	public void delete(Integer id) {

		Session session = Utils.getSessionFactory().openSession();

		Lot l = BDDmodify.Queries.veureLot(id);

		int n = l.getProducte().getStock() - l.getQuantitat();

		l.getProducte().setStock(n);

		try {
			session.remove(l);

			session.getTransaction().commit();
			session.close();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		} finally {
			session.close();
		}

	}

}
