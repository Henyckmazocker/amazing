package dao_not_copypasted;

import java.util.List;

import tastat.*;

public interface ILotDao extends IGenericDao<Lot,Integer>{

	void saveOrUpdate(Lot l);

	Lot get(Integer id);

	List<Lot> list();

	void delete(Integer id);
	
	boolean afegirLot(Lot l);
	
}
