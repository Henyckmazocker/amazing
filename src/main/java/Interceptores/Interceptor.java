package Interceptores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import BDDmodify.Queries;
import dao_not_copypasted.*;
import tastat.*;

public class Interceptor extends EmptyInterceptor {

	public void onDelete(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		System.out.println("He entrado al interceptor");
		PeticioProveidorDao ppd = new PeticioProveidorDao();

		if (entity instanceof Lot) {

			int n = ((Lot) entity).getProducte().getStock() - ((Lot) entity).getQuantitat();

			if (n < ((Lot) entity).getProducte().getStockMinim()) {

				PeticioProveidor pp = new PeticioProveidor(((Lot) entity).getProducte(), Queries.veureProveidor(1),
						((Lot) entity).getProducte().getTipus().ordinal(), new Date(), new Date(),
						Enum.valueOf(ComandaEstat.class, "PENDENT"));

				ppd.saveOrUpdate(pp);
				// <>
				System.out.println("PETICION ENVIADA");

			}
		}
	}

	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {

		System.out.println("interceptado");
		
		PeticioProveidorDao ppd = new PeticioProveidorDao();

		LotDao ldao = new LotDao();

		ProducteDao pdao = new ProducteDao();

		if (entity instanceof Comanda) {

			for (Producte p : ((Comanda) entity).getProducte()) {

				Object[] al = (Object[]) p.getLots().toArray();
				
				System.out.println("lote borrado");

				ldao.delete(((Lot) al[0]).getId());
				
				return true;
				
			}

		}
		return false;
	}
}
