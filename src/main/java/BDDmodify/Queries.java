package BDDmodify;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import tastat.*;

public class Queries {

	public static Producte veureProductes(Integer id) {

		Session session = Utils.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<Producte> lista = session.createQuery("FROM Producte").list();

		session.getTransaction().commit();
		session.close();

		for (Producte pr : lista) {
			if (pr.getCodiProducte()==id) {
				return pr;

			}
		}

		return null;
	}

	public static Comanda veureComandes(Comanda c) {

		Session session = Utils.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<Comanda> lista = session.createQuery("FROM Comanda").list();

		session.getTransaction().commit();
		session.close();

		for (Comanda co : lista) {
			if (co.getIdComanda() == (c.getIdComanda())) {
				return co;

			}
		}

		return null;
	}

	public static Lot veureLot(Integer id) {

		Session session = Utils.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<Lot> lista = session.createQuery("FROM Lot").list();

		session.getTransaction().commit();
		session.close();

		for (Lot l1 : lista) {
			if (l1.getId() == id) {
				return l1;

			}
		}

		return null;
	}

	public static Client veureClients(String c) {

		Session session = Utils.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<Client> lista = session.createQuery("FROM Client").list();

		session.getTransaction().commit();
		session.close();

		for (Client co : lista) {
			if (c.equals(co.getCIF())) {

				if (co.getCIF().equals((c))) {

					return co;

				}
			}
		}
		return null;

	}
	
	public static Proveidor veureProveidor(int c) {

		Session session = Utils.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<Proveidor> lista = session.createQuery("FROM Proveidor").list();

		session.getTransaction().commit();
		session.close();

		for (Proveidor co : lista) {
			if (c==co.getIdProveidor()) {


					return co;

				}
			}
		
		return null;

	}
	public static PeticioProveidor veurePeticioProveidor(int c) {

		Session session = Utils.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		List<PeticioProveidor> lista = session.createQuery("FROM PeticioProveidor").list();

		session.getTransaction().commit();
		session.close();

		for (PeticioProveidor co : lista) {
			if (c==co.getIdComanda()) {


					return co;

				}
			}
		
		return null;

	}
}
