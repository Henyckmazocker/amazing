package BDDmodify;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import Interceptores.Interceptor;

public class Utils {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	static MetadataSources sources;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {

			// exception handling omitted for brevityaa

			 serviceRegistry = new StandardServiceRegistryBuilder()
		                .configure("hibernate.cfg.xml")
		                .build();

		        sources = new MetadataSources( serviceRegistry );
		        
		        Metadata metadata = sources.getMetadataBuilder().build();
		        
		        ///comentar o descomentar la linea del interceptor
		        sessionFactory = metadata.getSessionFactoryBuilder()
		        		.applyInterceptor(new Interceptor())
		        		.build();
		        	
			
		}
		return sessionFactory;
	}

	public static Session getSession() {
		return session;
	}

	public static void setSession(Session session) {
		Utils.session = session;
	}

	public static ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	public static void setServiceRegistry(ServiceRegistry serviceRegistry) {
		Utils.serviceRegistry = serviceRegistry;
	}

	public static void setSessionFactory(SessionFactory sessionFactory) {
		Utils.sessionFactory = sessionFactory;
	}
}
